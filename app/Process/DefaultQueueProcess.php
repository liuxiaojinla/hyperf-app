<?php

namespace App\Process;

use Hyperf\Process\Annotation\Process;
use Xin\EasyQueue\Hyperf\ConsumerProcess;

#[Process(name: "DefaultQueueProcess")]
class DefaultQueueProcess extends ConsumerProcess
{
    /**
     * @var string
     */
    protected string $queue = 'default';
}
