<?php

namespace App\JsonRpc;

use Hyperf\RpcServer\Annotation\RpcService;

#[RpcService(name: "DemoService", protocol: "jsonrpc-http", server: "jsonrpc-http", publishTo: 'consul')]
class DemoService implements DemoServiceInterface
{
    // 实现一个加法方法，这里简单的认为参数都是 int 类型
    public function hello(string $name): string
    {
        echo "demo service hello call.\n";
        return "Hello, {$name}!";
    }

}
