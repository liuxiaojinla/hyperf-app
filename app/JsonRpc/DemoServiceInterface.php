<?php

namespace App\JsonRpc;

interface DemoServiceInterface
{
    public function hello(string $name): string;
}
