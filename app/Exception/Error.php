<?php

namespace App\Exception;

use Hyperf\Validation\ValidationException;
use Throwable;

final class Error
{
    /**
     * @param string $message
     * @return ValidationException
     */
    public static function validationException(string $message): ValidationException
    {
        return ValidationException::withMessages([
            'default' => $message,
        ]);
    }

    /**
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @return BusinessException
     */
    public static function businessException(string $message, int $code = 400, Throwable $previous = null): BusinessException
    {
        return new BusinessException($code, $message, $previous);
    }
}
