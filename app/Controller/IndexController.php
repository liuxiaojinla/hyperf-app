<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use App\JsonRpc\DemoServiceInterface;

class IndexController extends AbstractController
{
    /**
     * @return array
     */
    public function index(): array
    {
        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();

        /** @var DemoServiceInterface $demoService */
        $demoService = app(DemoServiceInterface::class);
        echo "hello call:", $demoService->hello($user), "\n";

        return [
            'method' => $method,
            'message' => "Hello {$user}.",
        ];
    }
}
