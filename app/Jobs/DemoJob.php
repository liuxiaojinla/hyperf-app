<?php

namespace App\Jobs;

use Hyperf\AsyncQueue\Job;
use Xin\EasyQueue\Dispatchable;
use Xin\EasyQueue\Queueable;
use Xin\Logger\Logger;

class DemoJob extends Job
{
    use Dispatchable, Queueable;

    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * @var array
     */
    private array $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function handle(): void
    {
        $this->logger = Logger::logger('demo.job')->enableStdout()->enableStdoutTimePrint();

        $this->logger->info('demo.');
    }
}
