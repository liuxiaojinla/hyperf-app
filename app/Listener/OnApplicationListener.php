<?php

namespace App\Listener;

use App\Listener\Concerns\EventSubscriber;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BeforeMainServerStart;
use Hyperf\Framework\Event\BeforeServerStart;
use Hyperf\Framework\Event\BootApplication;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Xin\Logger\Logger;

#[Listener]
class OnApplicationListener implements ListenerInterface
{
    use EventSubscriber;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * BootApplicationListener
     */
    public function __construct()
    {
        $this->logger = Logger::logger()->enableStdout();
    }

    /**
     * @return \class-string[]
     */
    public function listen(): array
    {
        return [
            BootApplication::class,
            ConsoleCommandEvent::class,
            BeforeMainServerStart::class,
            BeforeServerStart::class,
        ];
    }

    /**
     * @param BootApplication $event
     * @return void
     */
    public function bootApplication(BootApplication $event)
    {
    }
}
