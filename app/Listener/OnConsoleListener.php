<?php

namespace App\Listener;

use App\Listener\Concerns\EventSubscriber;
use Hyperf\Command\Event\AfterExecute;
use Hyperf\Command\Event\AfterHandle;
use Hyperf\Command\Event\BeforeHandle;
use Hyperf\Command\Event\FailToHandle;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Psr\Log\LoggerInterface;
use Xin\Logger\Logger;


#[Listener]
class OnConsoleListener implements ListenerInterface
{
    use EventSubscriber;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * BootApplicationListener
     */
    public function __construct()
    {
        $this->logger = Logger::logger()->enableStdout();
    }

    /**
     * @return \class-string[]
     */
    public function listen(): array
    {
        return [
            BeforeHandle::class,
            AfterHandle::class,
            AfterExecute::class,
            FailToHandle::class,
        ];
    }

}
