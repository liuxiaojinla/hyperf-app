<?php
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Listener;

use App\Listener\Concerns\EventSubscriber;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\AfterWorkerStart;
use Hyperf\Framework\Event\BeforeWorkerStart;
use Hyperf\Framework\Event\MainWorkerStart;
use Hyperf\Framework\Event\OnWorkerError;
use Hyperf\Framework\Event\OnWorkerExit;
use Hyperf\Framework\Event\OnWorkerStop;
use Hyperf\Framework\Event\OtherWorkerStart;
use Psr\Log\LoggerInterface;
use Xin\Hyperf\Fortify\Facades\Redis;
use Xin\Logger\Logger;

#[Listener]
class OnWorkerListener implements ListenerInterface
{
    use EventSubscriber;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * BootApplicationListener
     */
    public function __construct()
    {
        $this->logger = Logger::logger()->enableStdout();
    }

    /**
     * @return \class-string[]
     */
    public function listen(): array
    {
        return [
            BeforeWorkerStart::class,
            MainWorkerStart::class,
            OtherWorkerStart::class,
            AfterWorkerStart::class,
            OnWorkerExit::class,
            OnWorkerStop::class,
            OnWorkerError::class,
        ];
    }

    /**
     * @param MainWorkerStart $event
     * @return void
     */
    public function mainWorkerStart(MainWorkerStart $event)
    {
    }

    /**
     * @param AfterWorkerStart $event
     * @return void
     */
    public function afterWorkerStart(AfterWorkerStart $event)
    {
    }
}
