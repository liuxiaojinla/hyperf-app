<?php
declare(strict_types=1);

namespace App\Listener;

use Hyperf\Crontab\Event\AfterExecute;
use Hyperf\Crontab\Event\BeforeExecute;
use Hyperf\Crontab\Event\CrontabDispatcherStarted;
use Hyperf\Crontab\Event\FailToExecute;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Psr\Log\LoggerInterface;
use Xin\Logger\Logger;

/**
 * Class FailToExecuteCrontabListener
 * @package App\Listener
 * @Listener()
 */
#[Listener]
class OnCrontabListener implements ListenerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * DbQueryExecutedListener constructor.
     */
    public function __construct()
    {
        $this->logger = Logger::logger('crontab')->enableStdout();
    }

    /**
     * @return \class-string[]
     */
    public function listen(): array
    {
        return [
            AfterExecute::class,
            BeforeExecute::class,
            CrontabDispatcherStarted::class,
            FailToExecute::class,//监听定时任务执行失败
        ];
    }

    /**
     * @param object $event
     */
    public function process(object $event): void
    {
//        $this->logger->warning("任务执行失败", [
//            "class" => self::class,
//            "name" => $event->crontab->getName(),
//            "msg" => $event->throwable->getMessage(),
//            "trace" => $event->throwable->getTraceAsString(),
//        ]);
    }
}
