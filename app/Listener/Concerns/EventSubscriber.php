<?php

namespace App\Listener\Concerns;

use Hyperf\Stringable\Str;

trait EventSubscriber
{
    /**
     * @param object $event
     * @return void
     */
    public function process(object $event): void
    {
        $eventClassName = class_basename($event);
        $this->logger->info(static::class . ' => ' . $eventClassName);

        $method = Str::camel($eventClassName);
        if (method_exists($this, $method)) {
            $this->$method($event);
        }
    }
}
