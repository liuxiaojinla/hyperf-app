<?php

namespace App\Listener;

use App\Listener\Concerns\EventSubscriber;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Process\Event\AfterProcessHandle;
use Hyperf\Process\Event\BeforeProcessHandle;
use Psr\Log\LoggerInterface;
use Xin\Logger\Logger;


#[Listener]
class OnProcessListener implements ListenerInterface
{
    use EventSubscriber;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * BootApplicationListener
     */
    public function __construct()
    {
        $this->logger = Logger::logger()->enableStdout();
    }

    /**
     * @return \class-string[]
     */
    public function listen(): array
    {
        return [
            BeforeProcessHandle::class,
            AfterProcessHandle::class,
        ];
    }

}
