<?php

namespace App\Model;


/**
 * 记录日志
 *
 * Class Logs
 * @property string $name
 * @property string $content
 * @package App\Model
 */
class Log extends Model
{
    /**
     * @var string|null
     */
    protected ?string $table = "logs";

    /**
     * @var array
     */
    protected array $guarded = [];

}
