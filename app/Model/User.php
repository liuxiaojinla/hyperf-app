<?php

namespace App\Model;

/**
 * @property-read int $id
 */
class User extends Model
{
    /**
     * @var array|string[]
     */
    protected array $fillable = [
        'nickname',
        'avatar',
    ];

    /**
     * @var array|string[]
     */
    protected array $attributes = [
        'password' => '',
    ];

    /**
     * @var array|string[]
     */
    protected array $hidden = [
        'password',
    ];
}
