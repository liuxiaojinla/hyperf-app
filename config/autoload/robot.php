<?php
// +----------------------------------------------------------------------
// | 机器人设置
// +----------------------------------------------------------------------

use function Hyperf\Support\env;

return [
	// 定义相关默认配置
	'defaults' => [
		'robot' => env('ROBOT_DEFAULT_DRIVER', 'default'),
	],

	// 定义机器人的相关配置
	'robots' => [
		'default' => [
			'driver' => 'wework',
			'key' => env('ROBOT_DEFAULT_WEWORK_KEY', ''),
		],

		//
		'danger' => [
			'driver' => 'dingtalk',
			'key' => env('ROBOT_DEFAULT_DINGTALK_KEY', ''),
			'secret' => env('ROBOT_DEFAULT_DINGTALK_SECRET', ''),
			'title' => env('ROBOT.DINGTALK_TITLE', '提醒助手'),
		],
	],
];
