<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Hyperf\Watcher\Driver\FindNewerDriver;
use Hyperf\Watcher\Driver\FswatchDriver;
use Hyperf\Watcher\Driver\ScanFileDriver;

return [
    'driver' => FindNewerDriver::class,
    'bin' => PHP_BINARY,
    'watch' => [
        'dir' => ['app', 'config'],
        'file' => ['.env'],
        'scan_interval' => 1000,
    ],
    'ext' => ['.php', '.env'],
];
