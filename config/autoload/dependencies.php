<?php

declare(strict_types=1);

/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Xin\Logger\Logger;

return [
//    \Hyperf\Crontab\Strategy\StrategyInterface::class => Hyperf\Crontab\Strategy\WorkerStrategy::class,
//    \Hyperf\Crontab\Strategy\StrategyInterface::class => Hyperf\Crontab\Strategy\TaskWorkerStrategy::class,
//    \Hyperf\Crontab\Strategy\StrategyInterface::class => Hyperf\Crontab\Strategy\ProcessStrategy::class,
//    \Hyperf\Crontab\Strategy\StrategyInterface::class => Hyperf\Crontab\Strategy\CoroutineStrategy::class,
    \Hyperf\Crontab\LoggerInterface::class => function () {
        return Logger::logger('crontab')->enableStdout();
    },
];
