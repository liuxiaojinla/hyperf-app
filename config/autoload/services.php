<?php

declare(strict_types=1);

/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use function Hyperf\Support\env;

$registry = [
    'protocol' => env('SERVICE_PROTOCOL','consul'),
    'address' => env('SERVICE_ADDRESS','http://127.0.0.1:8500'),
];

return [
    'enable' => [
        'discovery' => env('SERVICE_DISCOVER_ENABLE', false),
        'register' => env('SERVICE_REGISTER_ENABLE', false),
    ],
    'consumers' => [
        [
            // name 需与服务提供者的 name 属性相同
            'name' => 'DemoService',
            // 服务接口名，可选，默认值等于 name 配置的值，如果 name 直接定义为接口类则可忽略此行配置，如 name 为字符串则需要配置 service 对应到接口类
            'service' => \App\JsonRpc\DemoServiceInterface::class,
            // 对应容器对象 ID，可选，默认值等于 service 配置的值，用来定义依赖注入的 key
            'id' => \App\JsonRpc\DemoServiceInterface::class,
            // 服务提供者的服务协议，可选，默认值为 jsonrpc-http
            // 可选 jsonrpc-http jsonrpc jsonrpc-tcp-length-check
            'protocol' => 'jsonrpc-http',
            // 负载均衡算法，可选，默认值为 random
            'load_balancer' => 'random',
            // 这个消费者要从哪个服务中心获取节点信息，如不配置则不会从服务中心获取节点信息
            'registry' => $registry,
            // 如果没有指定上面的 registry 配置，即为直接对指定的节点进行消费，通过下面的 nodes 参数来配置服务提供者的节点信息
//            'nodes' => [
//                ['host' => '127.0.0.1', 'port' => 9504],
//            ],
            // 配置项，会影响到 Packer 和 Transporter
            'options' => [
                'connect_timeout' => 5.0,
                'recv_timeout' => 5.0,
                'settings' => [
                    // 根据协议不同，区分配置
                    'open_eof_split' => true,
                    'package_eof' => "\r\n",
                    // 'open_length_check' => true,
                    // 'package_length_type' => 'N',
                    // 'package_length_offset' => 0,
                    // 'package_body_offset' => 4,
                ],
                // 重试次数，默认值为 2，收包超时不进行重试。暂只支持 JsonRpcPoolTransporter
                'retry_count' => 2,
                // 重试间隔，毫秒
                'retry_interval' => 100,
                // 使用多路复用 RPC 时的心跳间隔，null 为不触发心跳
                'heartbeat' => 30,
                // 当使用 JsonRpcPoolTransporter 时会用到以下配置
                'pool' => [
                    'min_connections' => 1,
                    'max_connections' => 32,
                    'connect_timeout' => 10.0,
                    'wait_timeout' => 3.0,
                    'heartbeat' => -1,
                    'max_idle_time' => 60.0,
                ],
            ],
        ]
    ],
    'providers' => [],
    'drivers' => [
        'consul' => [
            'uri' => env('CONSUL_URL', 'http://127.0.0.1:8500'),
            'token' => env('CONSUL_TOKEN', ''),
            'check' => [
                'deregister_critical_service_after' => env('CONSUL_CHECK_DEREGISTER_CRITICAL_SERVICE_AFTER', '90m'),
                'interval' => env('CONSUL_CHECK_INTERVAL', '1s'),
            ],
        ],
        'nacos' => [
            // nacos server url like https://nacos.hyperf.io, Priority is higher than host:port
            'url' => env('NACOS_URL', ''),
            // The nacos host info
            'host' => env('NACOS_HOST', '127.0.0.1'),
            'port' => env('NACOS_PORT', '8848'),
            // The nacos account info
            'username' => env('NACOS_USERNAME'),
            'password' => env('NACOS_PASSWORD'),
            'guzzle' => [
                'config' => null,
            ],
            'group_name' => env('NACOS_GROUP_NAME', 'api'),
            'namespace_id' => env('NACOS_NAMESPACE_ID', 'namespace_id'),
            'heartbeat' => env('NACOS_HEARTBEAT', 5),
            'ephemeral' => env('NACOS_EPHEMERAL', true),
            'cluster' => env('NACOS_CLUSTER', 'DEFAULT'),
            // Only support for nacos v2.
            'grpc' => [
                'enable' => env('NACOS_GRPC_ENABLE', false),
                'heartbeat' => env('NACOS_GRPC_HEARTBEAT', 10),
            ],
        ],
    ],
];
