<?php

use function Hyperf\Support\env;

return [
    // 是否开启定时任务
    'enable' => env('BOOT_CRONTAB', false),
    // 定时任务配置
    'crontab' => [
        // Callback类型定时任务（默认）
//        (new Crontab())->setName('Demo:Callback')->setRule('* * * * * *')->setCallback([App\Crontab\Demo::class, 'execute'])->setMemo('这是一个示例的定时任务'),
        // Command类型定时任务
//        (new Crontab())->setName('Demo:Command')->setType('command')->setRule('* * * * * *')->setCallback([
//            'command' => 'test',
//            // (optional) arguments
////            'argument' => 'barValue',
//            // (optional) options
////            '--message-limit' => 1,
//            // 记住要加上，否则会导致主进程退出
//            '--disable-event-dispatcher' => true,
//        ]),
        // Closure 类型定时任务 (仅在 Coroutine style server 中支持)
//        (new Crontab())->setName('Closure')->setType('closure')->setRule('* * * * * *')->setCallback(function () {
//            var_dump(date('Y-m-d H:i:s'));
//        }),
    ],
];
