<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */


return [
    App\Process\DefaultQueueProcess::class,
    App\Process\DemoQueueProcess::class,
    Hyperf\Crontab\Process\CrontabDispatcherProcess::class,
];
