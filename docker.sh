#!/bin/sh

# 定义 .env 文件的路径
ENV_FILE=".env"

# 检查 .env 文件是否存在
if [ -f "$ENV_FILE" ]; then
    # 加载环境变量
#    export $(cat "$ENV_FILE" | xargs)
    # 安全地加载环境变量
    export $(cat "$ENV_FILE" | xargs -I {} echo export {} | sed 's/#.*//g' | xargs)
else
    echo ".env 文件不存在: $ENV_FILE"
    exit 1
fi

# 镜像名称
IMAGE_NAME="hyperf-app-watcher:latest"
IMAGE_NAME="hyperf/hyperf:8.3-alpine-v3.19-swoole-5.1.3"
IMAGE_NAME="hyperf/hyperf:8.3-alpine-v3.19-swoole"

CURRENT_DIR=$(pwd)
echo "当前项目路径：$CURRENT_DIR  使用镜像：$IMAGE_NAME  应用名称：$APP_NAME"


# 判断容器是否存在，如果存在则重启容器，如果不存在则新创建容器
CONTAINER_ID=$(docker ps -aq --filter "name=^$APP_NAME")
if [ -z "$CONTAINER_ID" ]; then
    # 容器不存在，创建并启动新容器
    echo "容器不存在，已创建新容器：$APP_NAME"
    # --rm \
    docker run -it \
    --restart=always \
    --name $APP_NAME \
    --add-host="host.docker.internal:host-gateway" \
    -v $CURRENT_DIR:/opt/www \
    -p $DOCKER_PORT:9501 \
    -p $DOCKER_WEBSOCKET_PORT:9502 \
    -p $DOCKER_SERVICE_PORT:9504 \
    --privileged -u root \
    --entrypoint /bin/bash \
    $IMAGE_NAME \
    -c "cd /opt/www && exec /bin/bash"
else
    # 容器存在，重启容器
    docker restart $CONTAINER_ID
    echo "容器存在，已重启容器：$APP_NAME"

    # 进入容器
    docker exec -u root -it $CONTAINER_ID /bin/bash
fi
